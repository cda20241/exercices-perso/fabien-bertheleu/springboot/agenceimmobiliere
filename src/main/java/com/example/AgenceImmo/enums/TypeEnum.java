package com.example.AgenceImmo.enums;

public enum TypeEnum {
    IMMEUBLE,
    APPARTEMENT,
    MAISON;

    public static boolean contains(TypeEnum type) {
        for (TypeEnum value : values()) {
            if (value.equals(type)) {
                return true;
            }
        }
        return false;
    }
    }

