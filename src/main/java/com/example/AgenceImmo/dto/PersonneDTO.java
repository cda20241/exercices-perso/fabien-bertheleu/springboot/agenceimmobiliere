package com.example.AgenceImmo.dto;
import com.example.AgenceImmo.dto.BasePersonneDTO;
import com.example.AgenceImmo.dto.BienDTO;
import com.example.AgenceImmo.entity.PersonneEntity;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class PersonneDTO extends BasePersonneDTO {
    private List<BaseBienDTO> biens;

    public void mapper(PersonneEntity personne, List<BaseBienDTO> biens) {
        super.mapper(personne);
        this.biens = biens;
    }
}
