package com.example.AgenceImmo.dto;

import com.example.AgenceImmo.entity.BienEntity;
import com.example.AgenceImmo.repository.BienRepository;
import com.example.AgenceImmo.enums.TypeEnum;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@RequiredArgsConstructor

public class BaseBienDTO {
    private Long id;
    private String ville;


    public void mapper(BienEntity bienEntity){
        this.id = bienEntity.getId();
        this.ville = bienEntity.getVille();
    }

}
