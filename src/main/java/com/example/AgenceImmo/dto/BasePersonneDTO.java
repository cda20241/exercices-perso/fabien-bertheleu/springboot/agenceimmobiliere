package com.example.AgenceImmo.dto;


import com.example.AgenceImmo.entity.PersonneEntity;

import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor

public class BasePersonneDTO {
    private Long id;
    private String prenom;
    private String nom;

    public void mapper(PersonneEntity personne) {
        this.id = personne.getId();
        this.prenom = personne.getPrenom();
        this.nom = personne.getNom();
    }
}




