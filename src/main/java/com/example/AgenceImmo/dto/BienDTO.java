package com.example.AgenceImmo.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class BienDTO extends BaseBienDTO{
    private List<BasePersonneDTO> personnes;
}
