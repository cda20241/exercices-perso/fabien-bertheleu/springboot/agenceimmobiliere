package com.example.AgenceImmo.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtService {

    // peut également être déclarée dans le fichier application.properties
    private static final String SECRET_KEY = "3272357538782F413F4428472D4B6150645367566B5970337336763979244226";

    // méthode de récupération du nom d'utilisateur (login)
    public String extractUsername(String jwt) {
        return extractClaim(jwt, Claims::getSubject);
    }

    // méthode d'extraction d'une seule revendication ciblée
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    // méthode méthode de génération de token sans revendications supplémentaires (extraClaims)
    public String generateToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }

    // méthode de génération de token avec des revendications supplémentaires (extraClaims)
    public String generateToken(
            Map<String, Object> extraClaims,
            UserDetails userDetails
    ) {
        return Jwts
                .builder()
                .setClaims(extraClaims) // ajout de revendications supplémentaires
                .setSubject(userDetails.getUsername()) // ajout du nom de l'utilisateur qui nous permet d'identifier le propriétaire du token par la suite
                .setIssuedAt(new Date(System.currentTimeMillis())) // indique quand le token a été issu afin de pouvoir ensuite calculer quand le token expire
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 24)) // indique la durée de validité du token ici 24H + 1060 ms
                .signWith(getSignInKey(), SignatureAlgorithm.HS256) // récupération de la clé de signature du jeton JWT et déclaration de l'algorythme de signature
                .compact(); // génère et retourne le token
    }

    // méthode d'extraction de toutes les revendication (claims) du token JWT
    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignInKey() {
        // déclaration de l'algorythme de signature
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
    // méthode de validation de token. Vérifie si le token appartient bien à l'utilisateur émetteur
    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    // méthode de vérification de la date d'expiration d'un token
    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    //méthode d'extraction de la date d'expiration du token
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}