package com.example.AgenceImmo.config;

import com.example.AgenceImmo.repository.PersonneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.method.P;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfig {

    private final PersonneRepository personneRepository;

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> personneRepository.findUserByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Personne non trouvée"));
    }

    @Bean
    public AuthenticationProvider authenticationProvider() { // data access object chargé de récupérer les UserDetails, d'encoder le mdp, ...
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        // on déclare le service à utiliser pour récupérer les détails de l'utilisateur
        authProvider.setUserDetailsService(userDetailsService());
        // on déclare not password encoder
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}