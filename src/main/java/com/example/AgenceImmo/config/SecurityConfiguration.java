package com.example.AgenceImmo.config;
import jakarta.servlet.Filter;
import jakarta.servlet.Filter;
import com.example.AgenceImmo.enums.RoleEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    // final permet ici au filtre d'être automatiquement injecté par Spring
    private final JwtAuthentificationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{

        http
                // Spring documentation: Our recommendation is to use CSRF protection for any request that could be
                // processed by a browser by normal users. If you are only creating a service that is used by
                // non-browser clients, you will likely want to disable CSRF protection.
                // plus d'infos sur la protection CSRF avec Spring: https://www.baeldung.com/spring-security-csrf
                .csrf()
                .disable()
                // quand on créé un utilisateur ou lors du loggin nous n'avons pas besoin d'un token donc on doit
                // whitelister ces deux controlers afin de permmettre leur accès
                .authorizeHttpRequests()
                // liste des url des contrôlers à whitelister (** -> ici signifie tous les url commençant par
                // "/api/vi/auth/" c'est à dire ts les endpoints de notre AuthentificationController)
                .requestMatchers("/agenceimmo/auth/register").permitAll()
                .requestMatchers("/agenceimmo/auth/authenticate").permitAll()
                .requestMatchers("/agenceimmo/bien/Ajouter").hasAnyAuthority(RoleEnum.AGENT.toString(), RoleEnum.PROPRIETAIRE.toString())
                .requestMatchers("/agenceimmo/bien/Modifier/**").hasAnyAuthority(RoleEnum.AGENT.toString(), RoleEnum.PROPRIETAIRE.toString())
                .requestMatchers("/agenceimmo/bien/Effacer/**").hasAnyAuthority(RoleEnum.AGENT.toString(), RoleEnum.PROPRIETAIRE.toString())
                .requestMatchers("/agenceimmo/personne/ajouterIDpersonneIDbien/**").hasAuthority(RoleEnum.VENDEUR.toString())
                .requestMatchers("/agenceimmo/personne/supprimerIDpersonneIDbien/**").hasAuthority(RoleEnum.AGENT.toString())
//                .permitAll()
                // toute requête vers les autres contrôleurs a besoin d'un token
                .anyRequest().authenticated()
                .and()
                // configuration du management de sessions
                .sessionManagement()
                // définission de la façon dont souhaite créer notre session (ici stateless car nous voulons créer
                // une nouvelle session pour chaque requête
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // définition du fournisseur d'authentification
                .authenticationProvider(authenticationProvider)
                // ajout de notre avant le filtre d'authentification username and password
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}