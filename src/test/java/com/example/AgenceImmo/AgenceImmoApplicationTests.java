package com.example.AgenceImmo;

import com.example.AgenceImmo.controller.PersonneController;
import com.example.AgenceImmo.entity.PersonneEntity;
import com.example.AgenceImmo.repository.PersonneRepository;
import com.mysql.cj.x.protobuf.Mysqlx;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import com.example.AgenceImmo.enums.RoleEnum;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;



@SpringBootTest
class AgenceImmoApplicationTests {
	@Autowired
	private PersonneController personneController;

	@Autowired
	private PersonneRepository personneRepository;
	/**Ce test pour vérifier le fonctionnement de la fonction findByRole qui recherche dans dans la BDD une personne
	 * en prenant en paramètre l'énumération RoleEnum. Pour le test, je choisis l'attribut "propriétaire"
	 */
	@Test
	@Transactional
	void testFindByRole() {
			// Je créé mon Jeu de données
		PersonneEntity personneTest = new PersonneEntity();
		personneTest.setRole(RoleEnum.PROPRIETAIRE);
		personneRepository.save(personneTest);

		// Appel de la fonction pour obtenir la liste des personnes ayant le rôle spécifié
		List<PersonneEntity> resultat = personneRepository.findByRole(RoleEnum.PROPRIETAIRE);

		// Je vérifie si la liste contient la personne de test avec un boolean;
		assertTrue(resultat.contains(personneTest));
		////// Tester role qui n'existe pas

	}

	/**Ce test pour vérifier le fonctionnement de la fonction BienController.CalculRole() qui permet de donner le
	 * nombre de personne ayant un même role dans la liste. Les roles sont définis dans l'enumeration "RoleEnum"
	 * La fonction est un switch/case, je teste donc tous les cas.
	 */
	@Test
	@Transactional
	void  testCalculRole(){

		//Jeu de données
		PersonneEntity personneTest1 = new PersonneEntity();
		personneTest1.setRole(RoleEnum.PROPRIETAIRE);
		PersonneEntity  personneTest2 = new PersonneEntity();
		personneTest2.setRole(RoleEnum.AGENT);
		PersonneEntity personneTest3= new PersonneEntity();
		personneTest3.setRole(RoleEnum.VENDEUR);
		PersonneEntity personneTest4 = new PersonneEntity();
		personneTest4.setRole(RoleEnum.PROSPECT);
		personneRepository.save(personneTest1);
		personneRepository.save(personneTest2);
		personneRepository.save(personneTest3);
		personneRepository.save(personneTest4);
		HttpStatus attenduStatutOK = HttpStatus.OK;
		HttpStatus attenduNonOK = HttpStatus.BAD_REQUEST;
		String valeurAttendueProprietaire = "1 proprietaire dans la liste";
		String valeurAttendueAgent = "1 agent dans la liste";
		String valeurAttendueProspect ="1 prospect dans la liste";
		String valeurAttendueVendeur = "1 vendeur dans la liste";
		String ValeurAttendueEnNombre = "1";

		////////////////////Appel de la fonction en prenant la valeur proprietaire
		ResponseEntity<String> resultatproprio = personneController.calculRole("proprietaire");
		//test pour trouver la valeur de calcul dans la fonction, calcul étant dans le Body de ResponseEntity
		//le body est un String, on sépare (split) chaque mot en prenant l'espace en séparateur et on ajoute
		//chacun à une liste. On ressort ensuite la première valeur de la liste (la place [0])
		String[] testValeurProprietaire = resultatproprio.getBody().split(" ");
		System.out.print(testValeurProprietaire[0]);
		//on instancie le body en entier
		String testBodyProprietaire = resultatproprio.getBody();
        //////////////////Test du statut
		// Test du Statut
		assertEquals(resultatproprio.getStatusCode(), attenduStatutOK);
		// Test de la valeur de calcul
		assertEquals(testValeurProprietaire[0], ValeurAttendueEnNombre);
		//test de tout le body de la ResponseEntity
		assertEquals(testBodyProprietaire, valeurAttendueProprietaire);

		////////////////////Appel de la fonction en prenant la valeur Agent
		ResponseEntity<String> resultatAgent = personneController.calculRole("agent");
		String[] testValeurAgent = resultatAgent.getBody().split(" ");
		System.out.print(testValeurAgent[0]);
		String testBodyAgent = resultatAgent.getBody();
		//////////////////Test du statut
		// Test du Statut
		assertEquals(resultatAgent.getStatusCode(), attenduStatutOK);
		// Test de la valeur de calcul
		assertEquals(testValeurAgent[0], ValeurAttendueEnNombre);
		//test de tout le body de la ResponseEntity
		assertEquals(testBodyAgent, valeurAttendueAgent);

		////////////////////Appel de la fonction en prenant la valeur Prospect
		ResponseEntity<String> resultatProspect = personneController.calculRole("prospect");
		String[] testValeurProspect =resultatProspect.getBody().split(" ");
		System.out.print(testValeurProspect[0]);
		String testBodyProspect = resultatProspect.getBody();
		//////////////////Test du statut
		// Test du Statut
		assertEquals(resultatProspect.getStatusCode(), attenduStatutOK);
		// Test de la valeur de calcul
		assertEquals(testValeurProspect[0], ValeurAttendueEnNombre);
		//test de tout le body de la ResponseEntity
		assertEquals(testBodyProspect, valeurAttendueProspect);

		////////////////////Appel de la fonction en prenant la valeur Vendeur
		ResponseEntity<String> resultatVendeur = personneController.calculRole("vendeur");
		String[] testValeurVendeur =resultatVendeur.getBody().split(" ");
		System.out.print(testValeurVendeur[0]);
		String testBodyVendeur = resultatVendeur.getBody();
		//////////////////Test du statut
		// Test du Statut
		assertEquals(resultatVendeur.getStatusCode(), attenduStatutOK);
		// Test de la valeur de calcul
		assertEquals(testValeurVendeur[0], ValeurAttendueEnNombre);
		//test de tout le body de la ResponseEntity
		assertEquals(testBodyVendeur, valeurAttendueVendeur);

		////////////////////Appel de la fonction en prenant une autre valeur pas dans l'enumeration
		ResponseEntity<String> resultatAutre = personneController.calculRole("tireLesPinpons");
		// Test du statut
		assertEquals(resultatAutre.getStatusCode(), attenduNonOK);
	}
}




